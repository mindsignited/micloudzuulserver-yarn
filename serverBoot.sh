#!/bin/bash

##
## Startup script for Zuul Server.  This will set the needed environment variables and
## boot up the application using the spring boot fat jar that is the artifact of the
## production build.
##
set -e

TO_RUN=client-*
SPRING_OPTS=
if [[ "x$1" == "xfatJar" || "x$2" == "xfatJar" || "x$3" == "xfatJar" ]]; then
    TO_RUN=container-*SNAPSHOT
fi

if [[ "x$1" == "xprod" || "x$2" == "xprod" || "x$3" == "xprod" ]]; then
    SPRING_OPTS="--spring.profiles.active=production"
fi

## launch args
CONFIG_APPLICATION=zuul
CONFIG_APPLICATION_EXTENSION=jar
PATH_TO_EXECUTABLE=${CONFIG_APPLICATION}-dist # <-- no trailing slash here.
# jvm arguments added here
JVM_OPTS=""

if [[ "x$1" == "xdebug" || "x$2" == "xdebug" || "x$3" == "xdebug" ]]; then
    JVM_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5009"
fi

SHELL=
if [[ "x$1" == "xshell" || "x$1" == "xpush" || "x$1" == "xsubmit" || "x$1" == "xkill" || "x$1" == "xshutdown" || "x$1" == "xsubmitted" ]]; then
    SHELL=$@
    SPRING_OPTS=
fi

java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${TO_RUN}.${CONFIG_APPLICATION_EXTENSION} ${SPRING_OPTS} ${SHELL}

