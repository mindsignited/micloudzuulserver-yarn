#!/bin/bash

## launch args
export CONFIG_APPLICATION=micloudzuul
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=$(pwd)/${CONFIG_APPLICATION}-dist # <-- no trailing slash here.
# jvm arguments added here
export JVM_OPTS=""
