package com.mindsignited.container.filters.route;

import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;

/**
 * Created by nicholaspadilla on 3/25/15.
 */
@RefreshScope
@Component
public class ApplicationTokenRequestFilter extends AbstractUserGridTokenFilter {

    @Value("${appTokenPath}")
    private String appTokenPath;

    @Override
    public String getTokenRequestPath() {
        return appTokenPath;
    }

    @Override
    public Object performRun(String organizationName, String defaultApplicationName, RequestContext context) {
        String clientId = context.getRequest().getParameter("clientid");
        String clientSecret = context.getRequest().getParameter("clientsecret");
        Assert.hasText(clientId, "You must provide a 'clientid' in query parameters for this request.");
        Assert.hasText(clientSecret, "You must provide a 'clientsecret' in query parameters for this request.");
        context.set("requestURI", "/" + organizationName + "/" + defaultApplicationName + "/token");

        // current sessions query params.
        context.getRequestQueryParams().clear();
        context.getRequestQueryParams().put("grant_type", Collections.singletonList("client_credentials"));
        context.getRequestQueryParams().put("client_id", Collections.singletonList(clientId));
        context.getRequestQueryParams().put("client_secret", Collections.singletonList(clientSecret));
        return null;
    }
}
