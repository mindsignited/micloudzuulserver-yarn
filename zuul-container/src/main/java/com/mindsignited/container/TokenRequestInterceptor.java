package com.mindsignited.container;

import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import feign.RequestInterceptor;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by nicholaspadilla on 3/25/15.
 */
@Component
public class TokenRequestInterceptor implements RequestInterceptor {

    @Autowired
    private HttpServletRequest request;

    @Override
    public void apply(RequestTemplate template) {
        String header = request.getHeader("authorization");
        if(header == null || header.isEmpty()){
            if(request.getQueryString() == null || !request.getQueryString().contains("access_token")){
                throw new AuthorizationException("No Authorization found for request! Must provide Authorization header with Bearer token");
            }
            UriComponents uriComponents = UriComponentsBuilder.newInstance().query(request.getQueryString()).build();
            String token = uriComponents.getQueryParams().get("access_token").get(0);
            header = "Bearer " + token;
        }
        template.header("Authorization", header);
    }
}
