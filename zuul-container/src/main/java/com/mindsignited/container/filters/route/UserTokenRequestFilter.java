package com.mindsignited.container.filters.route;

import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;

/**
 * Created by nicholaspadilla on 3/25/15.
 */
@RefreshScope
@Component
public class UserTokenRequestFilter extends AbstractUserGridTokenFilter {

    @Value("${userTokenPath}")
    private String userTokenPath;

    @Override
    public String getTokenRequestPath() {
        return userTokenPath;
    }

    @Override
    public Object performRun(String organizationName, String applicationName, RequestContext context) {
        String username = context.getRequest().getParameter("username");
        String password = context.getRequest().getParameter("password");
        Assert.hasText(username, "You must provide a 'username' in query parameters for this request.");
        Assert.hasText(password, "You must provide a 'password' in query parameters for this request.");
        context.set("requestURI", "/" + organizationName + "/" + applicationName + "/token");

        // current sessions query params.
        context.getRequestQueryParams().clear();
        context.getRequestQueryParams().put("grant_type", Collections.singletonList("password"));
        context.getRequestQueryParams().put("username", Collections.singletonList(username));
        context.getRequestQueryParams().put("password", Collections.singletonList(password));
        return null;
    }
}
