package com.mindsignited.container.filters.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindsignited.container.AuthorizationException;
import com.mindsignited.container.JsonAny;
import com.netflix.config.DynamicBooleanProperty;
import com.netflix.config.DynamicIntProperty;
import com.netflix.config.DynamicPropertyFactory;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.constants.ZuulConstants;
import com.netflix.zuul.constants.ZuulHeaders;
import com.netflix.zuul.context.RequestContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.ZuulConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

/**
 * Created by nicholaspadilla on 3/26/15.
 */
@RefreshScope
@Component
public class ManageErrorFilter extends ZuulFilter {

    protected static final String SEND_ERROR_FILTER_RAN = "sendErrorFilter.ran";

    protected static final String ERROR_FILTER_RAN = "manageErrorFilter.ran";

    private static final Logger logger = Logger.getLogger(ManageErrorFilter.class);

    private static DynamicBooleanProperty INCLUDE_DEBUG_HEADER = DynamicPropertyFactory
            .getInstance().getBooleanProperty(ZuulConstants.ZUUL_INCLUDE_DEBUG_HEADER,
                    false);

    private static DynamicIntProperty INITIAL_STREAM_BUFFER_SIZE = DynamicPropertyFactory
            .getInstance().getIntProperty(ZuulConstants.ZUUL_INITIAL_STREAM_BUFFER_SIZE,
                    1024);

    private static DynamicBooleanProperty SET_CONTENT_LENGTH = DynamicPropertyFactory
            .getInstance().getBooleanProperty(ZuulConstants.ZUUL_SET_CONTENT_LENGTH,
                    false);

    @Value("${error.path:/error}")
    private String errorPath;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return -1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        // only forward to errorPath if it hasn't been forwarded to already
        return ctx.containsKey("throwable")
                && !ctx.getBoolean(ERROR_FILTER_RAN, false);
    }

    @Override
    public Object run() {
        try {
            RequestContext ctx = RequestContext.getCurrentContext();
            Throwable exception = (Exception) ctx.get("throwable");
            logger.warn("Error during filtering", Throwable.class.cast(exception));

            String root = "";
            String statusCode = "500";
            if (!exception.getCause().getMessage().contains("content:")) {
                // this is from usergrid - most likely so provide 401 and message
                if(exception.getCause().getMessage().contains("error_description")){
                    statusCode = "401";
                    JsonAny rootJson = mapper.readValue(exception.getCause().getMessage(), JsonAny.class);
                    root = (String)rootJson.getAnyObject().get("error_description");
                }else {
                    // error comes from our system
                    root = exception.getCause().getMessage();
                    if (exception.getCause() instanceof AuthorizationException) {
                        statusCode = "401";
                    }
                }
            } else {
                String[] messages = exception.getCause().getMessage().split(";");
                statusCode = messages[0].split(" ")[1].trim();
                String rootExceptionJson = messages[1].replace("content:", "");
                JsonAny rootJson = mapper.readValue(rootExceptionJson, JsonAny.class);
                root = (String)rootJson.getAnyObject().get("error_description");
            }
            writeResponse(root, statusCode);
        } catch (Exception ex) {
            ReflectionUtils.rethrowRuntimeException(ex);
        }
        return null;
    }

    private void writeResponse(String message, String statusCode) throws Exception {
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletResponse servletResponse = context.getResponse();
        servletResponse.setCharacterEncoding("UTF-8");
        OutputStream outStream = servletResponse.getOutputStream();
        try {
            String body = "{ \"statusCode\": \"" + statusCode + "\", \"message\": \"" + message + "\"}";
            writeResponse(new ByteArrayInputStream(body.getBytes()), outStream);
            return;
        } finally {
            try {
                outStream.flush();
                outStream.close();
            } catch (IOException ex) {

            }
        }
    }

    private void writeResponse(InputStream zin, OutputStream out) throws Exception {
        byte[] bytes = new byte[INITIAL_STREAM_BUFFER_SIZE.get()];
        int bytesRead = -1;
        while ((bytesRead = zin.read(bytes)) != -1) {
            try {
                out.write(bytes, 0, bytesRead);
                out.flush();
            }
            catch (IOException ex) {
                // ignore
            }
            // doubles buffer size if previous read filled it
            if (bytesRead == bytes.length) {
                bytes = new byte[bytes.length * 2];
            }
        }
    }
}
