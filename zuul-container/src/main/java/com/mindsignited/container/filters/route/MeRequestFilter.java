package com.mindsignited.container.filters.route;

import com.mindsignited.container.AuthorizationException;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;


/**
 * Created by nicholaspadilla on 3/26/15.
 */
@RefreshScope
@Component
public class MeRequestFilter extends AbstractUserGridTokenFilter  {

    @Value("${mePath}")
    private String mePath;

    @Override
    public String getTokenRequestPath() {
        return mePath;
    }

    @Override
    public Object performRun(String organizationName, String applicationName, RequestContext context) {

        String header = context.getRequest().getHeader("authorization");
        if(header == null && header.isEmpty()){
            throw new AuthorizationException("No Authorization found for request! Must provide Authorization header with Bearer token");
        }

        context.set("requestURI", "/" + organizationName + "/" + applicationName + "/users/me");

        return null;
    }
}
