package com.mindsignited.container.filters.route;

import com.google.common.net.InetAddresses;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Value;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by nicholaspadilla on 3/24/15.
 */
public abstract class AbstractUserGridTokenFilter extends ZuulFilter {

    @Value("${defaultOrganizationName}")
    private String defaultOrganizationName;

    @Value("${defaultApplicationName}")
    private String defaultApplicationName;

    @Value("${userGridClientBaseUrl}")
    private String userGridClientBaseUrl;

    public abstract String getTokenRequestPath();
    public abstract Object performRun(String organizationName, String applicationName, RequestContext context);

    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public int filterOrder() {
        return -1;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().getRequest().getRequestURL().indexOf(getTokenRequestPath()) > 0;
    }

    @Override
    public Object run() {
        Object ret = null;
        RequestContext context = RequestContext.getCurrentContext();
        try {
            URI requestURI = new URI(context.getRequest().getRequestURL().toString());
            String subDomainIsAppName = null;
            try {
                InetAddress inet = InetAddresses.forString(requestURI.getHost());
                // if we parsed correctly we are in dev environment most likely so just use default
                subDomainIsAppName = defaultApplicationName;
            } catch(IllegalArgumentException e) {
                // this will fail in production, but makes sure we don't have an IP here
                subDomainIsAppName = requestURI.getHost().substring(0, requestURI.getHost().indexOf("."));
            }

            // FIXME: this will make all requests expect json response, is that right?
            // everything is returned as json, so set the Accept header always
            context.addZuulRequestHeader("Accept", "application/json");
            context.addZuulResponseHeader("Content-Type", "application/json");

            ret = performRun(defaultOrganizationName, subDomainIsAppName, context);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
