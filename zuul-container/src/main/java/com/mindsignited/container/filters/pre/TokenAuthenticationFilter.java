package com.mindsignited.container.filters.pre;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindsignited.container.AuthenticationProperties;
import com.mindsignited.container.AuthorizationException;
import com.mindsignited.container.JsonAny;
import com.mindsignited.container.ZuulUserGridClient;
import com.mindsignited.container.filters.route.AbstractUserGridTokenFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.HttpServletRequestWrapper;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.ZuulConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.*;

/**
 * Created by nicholaspadilla on 3/25/15.
 */
@RefreshScope
@Component
@EnableConfigurationProperties(AuthenticationProperties.class)
public class TokenAuthenticationFilter extends AbstractUserGridTokenFilter {

    private static final Logger logger = Logger.getLogger(TokenAuthenticationFilter.class);

    @Autowired
    private ZuulUserGridClient zuulUserGridClient;

    @Autowired
    private AuthenticationProperties authenticationProperties;

    @Autowired
    private ObjectMapper mapper;

    private PathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String getTokenRequestPath() {
        throw new NotImplementedException();
    }

    @Override
    public Object performRun(String organizationName, String applicationName, RequestContext context) {

        String uriPath = context.getRequest().getRequestURI();

        // do we have an exact match
        AuthenticationProperties.AuthenticationDefinition def = this.authenticationProperties.getDefinitions().get(uriPath);
        if (def != null) {
            for (String role : def.getRoles()) {
                if (role.equalsIgnoreCase("PUBLIC")) {
                    return null;
                }
            }
        }

        def = getRolesForPathOrError(uriPath);
        // check for PUBLIC - else make calls to get user roles
        for (String role : def.getRoles()) {
            if (role.equalsIgnoreCase("PUBLIC")) {
                return null;
            }
        }

        String authorization = context.getRequest().getHeader("authorization");
        if (authorization == null || authorization.isEmpty()) {
            String query = context.getRequest().getQueryString();
            if(query == null || !query.contains("access_token")){
                throw new AuthorizationException("No Authorization found for request! Must provide Authorization header with Bearer token");
            }
        }

        // now get the roles for the user
        JsonAny userResponse = zuulUserGridClient.verifyUserToken(organizationName, applicationName);
        if (userResponse.getAnyObject().containsKey("exception")) {
            // we have an exception - so the user either has an expired token or the wrong orgId
            throw new AuthorizationException("Entity identified by access token doesn't have access to resource --> " + uriPath + " --> or access token has expired.");
        }
        try {
            // we will probably need to parse this back into a json string. :(
            String userString = mapper.writeValueAsString(userResponse.getAnyObject());
            context.addZuulRequestHeader("user", userString);
        } catch(JsonProcessingException e) {
            logger.error("Error parsing the user from JsonAny to a json string, just logging this as this shouldn't ever happen.");
        }

        JsonAny rolesResponse = zuulUserGridClient.getRolesForUser(organizationName, applicationName);
        List<Map> roles = (List<Map>) rolesResponse.getAnyObject().get("entities");
        if (roles == null || roles.isEmpty()) {
            throw new AuthorizationException("Entity identified by access token doesn't have access to resource --> " + uriPath);
        }
        boolean success = false;
        for (String role : def.getRoles()) {
            for (Map userRole : roles) {
                if (role.equalsIgnoreCase((String) userRole.get("roleName"))) {
                    success = true;
                }
            }
        }
        if (!success) {
            throw new AuthorizationException("Entity identified by access token doesn't have access to resource --> " + uriPath);
        }

        return null;
    }

    private AuthenticationProperties.AuthenticationDefinition getRolesForPathOrError(String uriPath) {
        AuthenticationProperties.AuthenticationDefinition def = null;
        List<String> matchingPatterns = new ArrayList<>();

        for (String registeredPattern : this.authenticationProperties.getDefinitions().keySet()) {
            if (pathMatcher.match(registeredPattern, uriPath)) {
                matchingPatterns.add(registeredPattern);
            }
        }
        String bestPatternMatch = null;
        Comparator<String> patternComparator = pathMatcher.getPatternComparator(uriPath);
        if (!matchingPatterns.isEmpty()) {
            Collections.sort(matchingPatterns, patternComparator);
            if (logger.isDebugEnabled()) {
                logger.debug("Matching patterns for request [" + uriPath + "] are " + matchingPatterns);
            }
            bestPatternMatch = matchingPatterns.get(0);
        }
        if (bestPatternMatch != null) {
            def = this.authenticationProperties.getDefinitions().get(bestPatternMatch);
        }
        if (def == null) {
            throw new IllegalArgumentException("No definition found for url path --> " + uriPath);
        }

        return def;
    }

}
