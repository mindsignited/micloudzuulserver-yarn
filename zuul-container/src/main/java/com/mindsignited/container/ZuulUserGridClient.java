package com.mindsignited.container;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by nicholaspadilla on 3/24/15.
 */
@FeignClient(url = "${userGridClientBaseUrl}")
public interface ZuulUserGridClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{organizationName}/{applicationName}/users/me", produces = {MediaType.APPLICATION_JSON_VALUE})
    JsonAny verifyUserToken(@PathVariable("organizationName") String organizationName, @PathVariable("applicationName") String applicationName);

    @RequestMapping(method = RequestMethod.GET, value = "/{organizationName}/{applicationName}", produces = {MediaType.APPLICATION_JSON_VALUE})
    JsonAny verifyApplicationToken(@PathVariable("organizationName") String organizationName, @PathVariable("applicationName") String applicationName);

    @RequestMapping(method = RequestMethod.GET, value = "/management/orgs/{organizationName}", produces = {MediaType.APPLICATION_JSON_VALUE})
    JsonAny verifyOrganizationToken(@PathVariable("organizationName") String organizationName);

    @RequestMapping(method = RequestMethod.GET, value = "/{organizationName}/{applicationName}/users/me/roles", produces = {MediaType.APPLICATION_JSON_VALUE})
    JsonAny getRolesForUser(@PathVariable("organizationName") String organizationName, @PathVariable("applicationName") String applicationName);

}
