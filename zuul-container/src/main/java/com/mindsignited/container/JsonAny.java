package com.mindsignited.container;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;


/**
 * Helper class to create arbitrary JSON objects.
 * @author navid
 *
 */
public class JsonAny {

	private Map<String, Object> anyObject = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAnyObject() {
		return anyObject;
	}

	@JsonAnySetter
	public void setAnyObject(String key, Object value){
		this.anyObject.put(key, value);
	}
	
}
