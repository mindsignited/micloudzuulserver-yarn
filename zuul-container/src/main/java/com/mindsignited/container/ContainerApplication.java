package com.mindsignited.container;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 *
 */
@SpringBootApplication
@EnableZuulProxy
@EnableFeignClients
public class ContainerApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(ContainerApplication.class).web(true).run(args);
    }
}
