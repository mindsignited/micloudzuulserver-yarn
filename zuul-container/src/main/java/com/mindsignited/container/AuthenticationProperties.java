package com.mindsignited.container;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nicholaspadilla on 3/25/15.
 */
@RefreshScope
@Data
@ConfigurationProperties("authenticationDefinitions")
public class AuthenticationProperties {

    private Map<String, AuthenticationDefinition> definitions = new LinkedHashMap<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AuthenticationDefinition {

        private List<String> roles = new ArrayList<String>();

    }
}
