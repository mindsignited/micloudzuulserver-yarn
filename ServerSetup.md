 How to setup the server for the Zuul Servers
========================================================

create one instance in vpc public subnet

create security group with these open <br/>
TCP PORT 8765 <br/>
TCP PORT 8766 <br/>
SSH PORT <br/>

fix the /etc/hostname file  it should look something like this `ip-10-0-2-238.ec2.internal`
Review the below transcript, on how to properly setup hosts/hostname in aws
    ok this has a script

    navidmitchell [2:22 AM]
    http://www.onepwr.org/2012/04/26/chef-recipe-to-setup-up-a-new-nodes-fqdn-hostname-etc-properly/

    navidmitchell [2:22 AM]
    says to drop into the /etc/network/if-up.d/

    navidmitchell [2:22 AM]
    to allow it to work after restarts

install java 8 - script at root of projects - run as sudo.


create /home/ubuntu/micloudzuul directory for zuul <br/>
add the setenv-prod.sh from the zuul server project to this directory <br/>
copy the ‘micloudzuul-container*’ jar, from micloudzuul-dist/ to this directory. <br/>
add the micloudzuul file from the zuul server project to /etc/init.d <br/>
make sure files are executable. <br/> <br/>

Install service — <br/>
sudo chmod +x /etc/init.d/micloudzuul <br/>
sudo chown root:root /etc/init.d/micloudzuul <br/>
sudo update-rc.d micloudzuul defaults <br/>
sudo update-rc.d micloudzuul enable <br/>

You might also want to update the eureka host configurations, in the setenv-prod.sh file.. see the file for more options.

start service - sudo service micloudzuul start
