#!/bin/bash

##
## Environment settings - they are all defaults and not needed but are available.
##


export APP_CONTAINER_MEMORY=2g
export APP_CONTAINER_VIRTUAL_CORES=1
export APP_CLIENT_MEMORY=64MB

export SERVICE_PORT=8765
export MGMT_SERVICE_PORT=8766
export EUREKA_HOST_PORT=8761
export EUREKA_HOSTNAME=discovery.mthinx.com
export EUREKA_PEER_HOST_PORT=8761
export EUREKA_PEER_HOSTNAME=discovery-peer.mthinx.com
export HADOOP_HOSTNAME=localhost
export HADOOP_HOST_PORT=8020
export HOST=127.0.0.1


##
## JUST LAUNCHER ARGS - only needed for the init.d script.
##

## launch args
export CONFIG_APPLICATION=micloudzuul
export CONFIG_APPLICATION_EXTENSION=jar
export PATH_TO_EXECUTABLE=/home/ubuntu/${CONFIG_APPLICATION} # <-- no trailing slash here.
# jvm arguments added here
export JVM_OPTS="-server -Xmx3072M -Xms3072M -XX:+UseConcMarkSweepGC -XX:+AggressiveOpts "
export SPRING_OPTS="--spring.profiles.active=production"
